from connection_manager.nmcli_interface.manager import scan_wifi, connect_to_saved_network, connect_to_new_network
from python_gui.ui.user_interface import UserInterface
from python_gui.utils.input.input import ask_and_get_answered
from .__consts__ import WIFI_PASSWORD_QUESTION, SUCCESSFUL_CONNECTION
from python_gui.utils.printer.printer import main_print


def start_ui():
    """
    Starts the wifi picking ui
    :return: None
    """
    while True:
        ui = UserInterface(scan_wifi(), True)
        _connect_to_wifi(ui.start_choosing())


def _connect_to_wifi(ssid):
    """
    Tries to connect to given ssid and prompts if needed
    :param ssid: ssid to connect to
    :return: None
    """
    if not connect_to_saved_network(ssid.ssid):
        if connect_to_new_network(ssid.ssid, ask_and_get_answered(WIFI_PASSWORD_QUESTION)):
            main_print(SUCCESSFUL_CONNECTION)
    else:
        main_print(SUCCESSFUL_CONNECTION)





