
WIFI_SCAN_COMMAND = "nmcli d wifi list"

SAVED_NETWORK_COMMAND = "nmcli con up '{ap}'"
NEW_NETWORK_COMMAND = "nmcli device wifi connect '{ap}' password {password}"

NEW_NETWORK_PASSWORD_QUESTION = "Please enter the password"
