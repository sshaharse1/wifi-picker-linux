from .__consts__ import WIFI_SCAN_COMMAND, SAVED_NETWORK_COMMAND, NEW_NETWORK_COMMAND
import os
from connection_manager.parser.parser.parser import scan_to_classes,\
    _nmcli_saved_connection_succeeded,\
    _nmcli_new_connection_succeeded


def scan_wifi():
    """
    Scans for wifi_network networks, if you are connected it will only show the connected network
    :return: parsed output
    """
    scan_output = os.popen(WIFI_SCAN_COMMAND).read()
    return scan_to_classes(scan_output)


def connect_to_saved_network(ssid):
    """
    Tries to connect to a saved network
    :param ssid: ssid of saved network
    :return: boolean if connection succeeded
    """
    connection = os.popen(SAVED_NETWORK_COMMAND.format(ap=ssid))
    return not connection.close()


def connect_to_new_network(ssid, password):
    """
    Connects to the given ssid with the given password
    :param ssid: ssid to connect to
    :param password: password to use
    :return: boolean if connection succeeded
    """
    connection = os.popen(NEW_NETWORK_COMMAND.format(ap=ssid, password=password)).read()
    return _nmcli_new_connection_succeeded(connection)
