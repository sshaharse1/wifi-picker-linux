from connection_manager.parser.wifi_network.wifi import WifiNetwork
from .__consts__ import NMCLI_SCAN_TABLE_KEYS_SPOT, NMCLI_SCAN_TABLE_SPLIT_KEY,\
    NMCLI_CONN_UP_SUCCEED_REGEX, NMCLI_DEVICE_WIFI_CONNECT_SUCCEED_REGEX
import re


def scan_to_classes(scan_output):
    """
    Parses the nmcli scanner and converts it to classes
    :param scan_output: string scan output
    :return: array of WifiNetwork
    """
    wifi_networks = []
    scan_output = _remove_table_keys(_split_to_lines(scan_output))
    for line in scan_output:
        wifi_networks.append(_line_to_class(line))
    return wifi_networks


def _remove_table_keys(scan_output):
    """
    nmcli shows the wifi scan with table keys at the top, this removes them
    :param scan_output: nmcli scan_output
    :return: scan output with the table keys
    """
    del scan_output[0]
    return scan_output


def _split_to_lines(scan_output):
    """
    Splits the scan output to lines
    :param scan_output: scan output to split
    :return: array of each line of the scan output
    """
    return _remove_empty_elements_in_array(scan_output.split(NMCLI_SCAN_TABLE_SPLIT_KEY))


def _line_to_class(line_output):
    """
    Converts an nmcli table output line to a WifiNetwork class
    :param line_output: a line out of nmcli table output
    :return: WifiNetwork class
    """
    line_output = _remove_empty_elements_in_array(_split_line_to_parameters(line_output))
    return WifiNetwork(line_output)


def _split_line_to_parameters(line_output):
    """
    split a line of the nmcli table to the different parameters
    :param line_output: line of the nmcli table
    :return: array of parameters
    """
    line_output = line_output.split("  ")
    line_output = _strip_elements_in_array(line_output)
    line_output = _remove_empty_elements_in_array(line_output)
    return line_output


def _remove_empty_elements_in_array(arr):
    """
    Removes all empty elements in given array
    :param arr: array to remove empty elements from
    :return: array without the empty elements
    """
    new_arr = []
    for element in arr:
        if element:
            new_arr.append(element)
    return new_arr


def _nmcli_saved_connection_succeeded(nmcli_output):
    """
    Checks if the nmcli "conn up" command worked
    :param nmcli_output: nmcli conn up output
    :return: boolean True if succeeded else False
    """
    pattern = re.compile(NMCLI_CONN_UP_SUCCEED_REGEX)
    return pattern.match(nmcli_output) is None


def _nmcli_new_connection_succeeded(nmcli_output):
    """
    Checks if the nmcli device wifi connect command succeeded
    :param nmcli_output: nmcli wifi connect command output
    :return: boolean True if succeeded else False
    """
    pattern = re.compile(NMCLI_DEVICE_WIFI_CONNECT_SUCCEED_REGEX)
    return pattern.match(nmcli_output) is None


def _strip_elements_in_array(arr):
    """
    Strips all elements in given array
    :param arr: array to strip elements in
    :return: stripped elements
    """
    stripped_array = []
    for element in arr:
        stripped_array.append(element.strip())
    return stripped_array
