from .__consts__ import IN_USE_SPOT, SSID_SPOT, MODE_SPOT, \
    CHANNEL_SPOT, RATE_SPOT, SIGNAL_SPOT, BARS_SPOT, SECURITY_SPOT, WIFI_STR_FORMAT, BARS_FORMAT


class WifiNetwork(object):
    def __init__(self, in_use, ssid, mode, channel, rate, signal, bars, security):
        """
        Represents a wifi network, see "nmcli d wifi list" for more info
        :param in_use: If the network is currently in use
        :param ssid: ssid of the network
        :param mode: network mode
        :param channel: channel that network is in
        :param rate: the bandwith of the network
        :param signal: signal of the network
        :param bars: nicer representation of the signal
        :param security: security type of the network
        """
        self.in_use = in_use
        self.ssid = ssid
        self.mode = mode
        self.channel = channel
        self.rate = rate
        self.signal = int(signal)
        self.bars = bars
        self.security = security
        self.pretty_bars = BARS_FORMAT[int((self.signal / 20) - 1)]

    def __init__(self, param_arr):
        """
        Converts an array of parameters to a WifiNetwork
        :param param_arr: array to create from
        """
        if len(param_arr) == 8:
            self.in_use = param_arr[IN_USE_SPOT]
            modifier = 0
        else:
            modifier = 1
        self.ssid = param_arr[SSID_SPOT - modifier]
        self.mode = param_arr[MODE_SPOT - modifier]
        self.channel = param_arr[CHANNEL_SPOT - modifier]
        self.rate = param_arr[RATE_SPOT - modifier]
        self.signal = int(param_arr[SIGNAL_SPOT - modifier])
        self.bars = param_arr[BARS_SPOT - modifier]
        self.security = param_arr[SECURITY_SPOT - modifier]
        self.pretty_bars = BARS_FORMAT[int((self.signal / 20) - 1)]

    def __repr__(self):
        return WIFI_STR_FORMAT.format(ssid=self.ssid, bars=self.pretty_bars)
