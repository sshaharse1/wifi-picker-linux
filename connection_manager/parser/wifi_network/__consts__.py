
# Param array spots
IN_USE_SPOT = 0
SSID_SPOT = 1
MODE_SPOT = 2
CHANNEL_SPOT = 3
RATE_SPOT = 4
SIGNAL_SPOT = 5
BARS_SPOT = 6
SECURITY_SPOT = 7

WIFI_STR_FORMAT = "{ssid} {bars}"

BARS_FORMAT = ["#####", "####-", "###--", "##---", "#----"]
